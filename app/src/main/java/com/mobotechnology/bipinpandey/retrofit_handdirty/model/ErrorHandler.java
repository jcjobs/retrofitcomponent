package com.mobotechnology.bipinpandey.retrofit_handdirty.model;

public class ErrorHandler extends Exception {

    public String  errorMessage;
    public int errorCode;
    public String errorCodeString;

    public ErrorHandler(int code, String codeString, String  message, Throwable cause) {
        super(cause);
        errorCode = code;
        errorCodeString = codeString;
        errorMessage = message;
    }
}