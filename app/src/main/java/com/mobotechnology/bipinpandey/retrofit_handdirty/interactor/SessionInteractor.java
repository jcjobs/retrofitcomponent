package com.mobotechnology.bipinpandey.retrofit_handdirty.interactor;

import android.content.Context;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mobotechnology.bipinpandey.retrofit_handdirty.Utilities.AnnotatedDeserializer;
import com.mobotechnology.bipinpandey.retrofit_handdirty.Utilities.SecurityHandler;
import com.mobotechnology.bipinpandey.retrofit_handdirty.Utilities.Utilities;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces.GenericResponseCallbacks;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces.ServiceConnectorInferface;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces.ValidateUserCallBacks;
import com.mobotechnology.bipinpandey.retrofit_handdirty.model.CUser;
import com.mobotechnology.bipinpandey.retrofit_handdirty.model.ErrorHandler;
import com.mobotechnology.bipinpandey.retrofit_handdirty.model.GenericResponseWS;
import com.mobotechnology.bipinpandey.retrofit_handdirty.network.ServiceConnector;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;

public class SessionInteractor implements ServiceConnectorInferface {


    public static final String Register = "0";
    public static final String Validate = "1";
    @StringDef({Register, Validate})
    @Retention(RetentionPolicy.SOURCE)
    public  @interface ManagementValidationType {}


    public static final String NewSession = "0";
    public static final String PasswordChange = "1";
    @StringDef({NewSession, PasswordChange})
    @Retention(RetentionPolicy.SOURCE)
    public  @interface ValdationType {}



    private static final int none = 0;
    private static final int validaCliente = 1;
    private static final int registraUsuario = 2;
    private static final int validaUsuario = 3;
    private static final int validaCredenciales = 4;
    private static final int cambiaPassword = 5;
    private static final int cierraSesion = 6;
    @IntDef({validaCliente, registraUsuario, validaUsuario, validaCredenciales, cambiaPassword, cierraSesion, none})
    @Retention(RetentionPolicy.SOURCE)
    private  @interface SessionQueryType {}


    private int currentTransaction = none;


    private Context currentContext;
    public SessionInteractor(Context context) {
        this.currentContext = context;
    }


    public void makeValidaClienteWith(String session, String token, @NonNull @ManagementValidationType String valdationType, String curpRFC, String fechaNacimiento, @NonNull String cuenta, String numCliente) {

        this.currentTransaction = validaCliente;

        String WS_FUNCTION = "validaCliente";

        Map<String, String> genericParams = new HashMap<>();
        genericParams.put("canal","BML");
        genericParams.put("tipoValidacion", valdationType);


        if(valdationType.equals(Register)){
            genericParams.put("sesion", session);
            genericParams.put("numCliente", numCliente);

        }
        else{
            genericParams.put("curpRFC", curpRFC);
            genericParams.put("fechaNacimiento", fechaNacimiento);
        }


        Map<String, Object> params = new HashMap<>();
        params.put("datosGenerales",genericParams);
        params.put("cuenta",cuenta);



        ServiceConnector serviceConnector = new ServiceConnector(this, this.currentContext);
        serviceConnector.makeConnectionWithParameters(WS_FUNCTION, params, ServiceConnector.POST);

    }


    private ValidateUserCallBacks validateUserCallBacks;
    public void makeValidaUsuarioWith(String token, @NonNull String cveUsuario, String phoneNumber, @NonNull @ValdationType String valdationType, ValidateUserCallBacks validateUserCallBacks){
        this.validateUserCallBacks = validateUserCallBacks;

        this.currentTransaction = validaUsuario;

        String WS_FUNCTION = "validaUsuario";


        String encryptedUserKey = SecurityHandler.bin2hex(SecurityHandler.getHash(cveUsuario));
        String deviceIp = Utilities.getIPAddress(true);

        Map<String, String> genericParams = new HashMap<>();
        genericParams.put("ipidmovil",deviceIp);
        genericParams.put("numTelefono",phoneNumber);
        genericParams.put("canal","BML");
        genericParams.put("tipoValidacion", valdationType);
        genericParams.put("cveUsuario", encryptedUserKey);


        Map<String, Object> params = new HashMap<>();
        params.put("datosGenerales",genericParams);
        params.put("token", token == null ? "" : token);

        ServiceConnector serviceConnector = new ServiceConnector(this, this.currentContext);
        serviceConnector.makeConnectionWithParameters(WS_FUNCTION, params, ServiceConnector.POST);

    }


    private GenericResponseCallbacks genericCallbacks;
    public void makeCambiaPasswordWith(@NonNull String sesion, String token, @NonNull String numTelefono, @NonNull String tipoCambio, String password, @NonNull String nuevoPassword,
                                       @NonNull String confirmaPassword, @NonNull String codigoConfirmacion, GenericResponseCallbacks genericCallbacks){
        this.genericCallbacks = genericCallbacks;

        this.currentTransaction = cambiaPassword;

        String WS_FUNCTION = "cambiaPassword";


        String encryptedNewPassword = SecurityHandler.bin2hex(SecurityHandler.getHash(nuevoPassword));
        String encryptedConfirmPassword = SecurityHandler.bin2hex(SecurityHandler.getHash(confirmaPassword));

        String deviceIp = Utilities.getIPAddress(true);

        Map<String, String> genericParams = new HashMap<>();
        genericParams.put("sesion", sesion);
        genericParams.put("ipidmovil",deviceIp);
        genericParams.put("numTelefono",numTelefono);
        genericParams.put("canal","BML");
        genericParams.put("tipoValidacion","0");


        Map<String, Object> params = new HashMap<>();
        params.put("datosGenerales",genericParams);
        params.put("token", token == null ? "" : token);
        params.put("tipoCambio", tipoCambio);
        params.put("tipnuevoPasswordoCambio", nuevoPassword);
        params.put("confirmaPassword", encryptedNewPassword);
        params.put("confirmaPassword", encryptedConfirmPassword);
        params.put("codigoConfirmacion", codigoConfirmacion);


        if(tipoCambio.equals("PN")){
            String encryptedPassword = SecurityHandler.bin2hex(SecurityHandler.getHash(password));
            params.put("Password", encryptedPassword);
        }


        ServiceConnector serviceConnector = new ServiceConnector(this, this.currentContext);
        serviceConnector.makeConnectionWithParameters(WS_FUNCTION, params, ServiceConnector.POST);

    }



    public void makeCierreSesionWith(String sesion, String token, String numTelefono, @NonNull String lastSessionDate){

        this.currentTransaction = cierraSesion;

        String WS_FUNCTION = "cierreSesion";


        String deviceIp = Utilities.getIPAddress(true);

        Map<String, String> genericParams = new HashMap<>();
        genericParams.put("sesion", sesion);
        genericParams.put("ipidmovil",deviceIp);
        genericParams.put("numTelefono",numTelefono);
        genericParams.put("canal","BML");
        genericParams.put("tipoValidacion","0");



        Map<String, Object> params = new HashMap<>();
        params.put("datosGenerales",genericParams);
        params.put("token", token == null ? "" : token);
        params.put("FechaCierre", lastSessionDate);

        ServiceConnector serviceConnector = new ServiceConnector(this, this.currentContext);
        serviceConnector.makeConnectionWithParameters(WS_FUNCTION, params, ServiceConnector.POST);

    }





    @Override
    public void requestReturnedData(String jsonString, ErrorHandler errorHandler) {

        switch (this.currentTransaction){
            case none:
                break;
            case 1:

                JsonParser parser = new JsonParser();
                JsonObject jsonObjectResult = parser.parse(jsonString).getAsJsonObject();

                JsonElement resultado = jsonObjectResult.get("resultado");
                JsonElement descripcionResultado = jsonObjectResult.get("descripcionResultado");

                break;
            case validaUsuario:

                Gson gson = new GsonBuilder()
                        .registerTypeAdapter(CUser.class, new AnnotatedDeserializer<CUser>())
                        .create();

                CUser currentUser = gson.fromJson(jsonString, CUser.class);

                validateUserCallBacks.onResponse(currentUser);
                //validateUserCallBacks.onError(errorHandler);

                break;
            default://[makeCierreSesionWith, makeCambiaPasswordWith]

                Gson gsonGenericResponse = new GsonBuilder()
                        .registerTypeAdapter(GenericResponseWS.class, new AnnotatedDeserializer<GenericResponseWS>())
                        .create();
                GenericResponseWS wsResponse = gsonGenericResponse.fromJson(jsonString, GenericResponseWS.class);

                if(errorHandler.errorCodeString.equals("0000")){
                    this.genericCallbacks.onResponse("");
                }
                else{
                    this.genericCallbacks.onError(errorHandler);
                }
                break;

        }


    }


}
