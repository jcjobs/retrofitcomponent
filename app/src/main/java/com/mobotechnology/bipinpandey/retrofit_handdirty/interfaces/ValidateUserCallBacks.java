package com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces;

import com.mobotechnology.bipinpandey.retrofit_handdirty.model.CUser;
import com.mobotechnology.bipinpandey.retrofit_handdirty.model.ErrorHandler;

public interface ValidateUserCallBacks {
    public void onResponse(CUser currentUser);
    public void onError(ErrorHandler error);
}
