package com.mobotechnology.bipinpandey.retrofit_handdirty.model;

import com.google.gson.annotations.SerializedName;

public class CAccount {

    @SerializedName("idAccount")
    private double idAccount;
    @SerializedName("amount")
    private double amount;
    @SerializedName("accountName")
    private String accountName;
    @SerializedName("account")
    private String account;
    @SerializedName("realName")
    private String realName;
    @SerializedName("isOwn")
    private Boolean isOwn;
    @SerializedName("bank")
    private String bank;
    @SerializedName("isFavourite")
    private Boolean isFavourite;
    @SerializedName("registerType")
    private String registerType;
    @SerializedName("email")
    private String email;
    @SerializedName("cellphone")
    private String cellphone;

    @SerializedName("maxTransferAmount")
    private double maxTransferAmount;
    @SerializedName("maxTransferOperations")
    private int maxTransferOperations;

    @SerializedName("rfc")
    private String rfc;
    @SerializedName("curp")
    private String curp;

    public CAccount(double idAccount, double amount, String accountName, String account, String realName, Boolean isOwn, String bank, Boolean isFavourite, String registerType, String email, String cellphone, double maxTransferAmount, int maxTransferOperations, String rfc, String curp) {
        this.idAccount = idAccount;
        this.amount = amount;
        this.accountName = accountName;
        this.account = account;
        this.realName = realName;
        this.isOwn = isOwn;
        this.bank = bank;
        this.isFavourite = isFavourite;
        this.registerType = registerType;
        this.email = email;
        this.cellphone = cellphone;
        this.maxTransferAmount = maxTransferAmount;
        this.maxTransferOperations = maxTransferOperations;
        this.rfc = rfc;
        this.curp = curp;
    }

    public double getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(double idAccount) {
        this.idAccount = idAccount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Boolean getOwn() {
        return isOwn;
    }

    public void setOwn(Boolean own) {
        isOwn = own;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Boolean getFavourite() {
        return isFavourite;
    }

    public void setFavourite(Boolean favourite) {
        isFavourite = favourite;
    }

    public String getRegisterType() {
        return registerType;
    }

    public void setRegisterType(String registerType) {
        this.registerType = registerType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public double getMaxTransferAmount() {
        return maxTransferAmount;
    }

    public void setMaxTransferAmount(double maxTransferAmount) {
        this.maxTransferAmount = maxTransferAmount;
    }

    public int getMaxTransferOperations() {
        return maxTransferOperations;
    }

    public void setMaxTransferOperations(int maxTransferOperations) {
        this.maxTransferOperations = maxTransferOperations;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }
}
