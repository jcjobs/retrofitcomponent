package com.mobotechnology.bipinpandey.retrofit_handdirty.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.mobotechnology.bipinpandey.retrofit_handdirty.R;
import com.mobotechnology.bipinpandey.retrofit_handdirty.adapter.NoticeAdapter;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces.ValidateUserCallBacks;
import com.mobotechnology.bipinpandey.retrofit_handdirty.model.CUser;
import com.mobotechnology.bipinpandey.retrofit_handdirty.model.ErrorHandler;
import com.mobotechnology.bipinpandey.retrofit_handdirty.model.Notice;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces.ResultInterface;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interactor.ResultInteractor;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interactor.SessionInteractor;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity{

    private NoticeAdapter adapter;
    private RecyclerView recyclerView;

    private ResultInteractor resultInteractor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        resultInteractor = new ResultInteractor(getApplicationContext());
        resultInteractor.makeGetNoticeList("", "","", new ResultInterface() {
            @Override
            public void onResponse(ArrayList<Notice> noticeList) {
                generateNoticeList(noticeList);
            }

            @Override
            public void onError(ErrorHandler error) {
                Toast.makeText(MainActivity.this, error.errorMessage, Toast.LENGTH_SHORT).show();
            }
        });




    }


    /** Method to generate List of notice using RecyclerView with custom adapter*/
    private void generateNoticeList(ArrayList<Notice> noticeArrayList) {
        recyclerView = findViewById(R.id.recycler_view_notice_list);
        adapter = new NoticeAdapter(noticeArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }


}
