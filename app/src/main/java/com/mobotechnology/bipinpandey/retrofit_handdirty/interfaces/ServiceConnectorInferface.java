package com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces;

import com.mobotechnology.bipinpandey.retrofit_handdirty.model.ErrorHandler;

public interface ServiceConnectorInferface {
    public void requestReturnedData(String jsonString, ErrorHandler errorHandler);
}
