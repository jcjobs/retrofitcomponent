package com.mobotechnology.bipinpandey.retrofit_handdirty.model;

import com.google.gson.annotations.SerializedName;

public class CInvestment {

    @SerializedName("inversion")
    private Double inversion;

    @SerializedName("account")
    private String account;

    @SerializedName("currentAmount")
    private Double currentAmount;

    @SerializedName("currentDay")
    private Double currentDay;

    @SerializedName("timeLapse")
    private Double timeLapse;

    @SerializedName("rate")
    private Double rate;

    @SerializedName("investType")
    private Double investType;

    @SerializedName("isExpired")
    private Boolean isExpired;

    @SerializedName("estimated")
    private Double estimated;
    @SerializedName("expirationDate")
    private String expirationDate;
    @SerializedName("isRenovable")
    private Boolean isRenovable;
    @SerializedName("currentProfit")
    private Double currentProfit;

    @SerializedName("maxRetreats")
    private int maxRetreats;
    @SerializedName("currentRetreats")
    private int currentRetreats;
    @SerializedName("nextCut")
    private String nextCut;


    public CInvestment(Double inversion, String account, Double currentAmount, Double currentDay, Double timeLapse, Double rate, Double investType, Boolean isExpired, Double estimated, String expirationDate, Boolean isRenovable, Double currentProfit, int maxRetreats, int currentRetreats, String nextCut) {
        this.inversion = inversion;
        this.account = account;
        this.currentAmount = currentAmount;
        this.currentDay = currentDay;
        this.timeLapse = timeLapse;
        this.rate = rate;
        this.investType = investType;
        this.isExpired = isExpired;
        this.estimated = estimated;
        this.expirationDate = expirationDate;
        this.isRenovable = isRenovable;
        this.currentProfit = currentProfit;
        this.maxRetreats = maxRetreats;
        this.currentRetreats = currentRetreats;
        this.nextCut = nextCut;
    }


    public Double getInversion() {
        return inversion;
    }

    public void setInversion(Double inversion) {
        this.inversion = inversion;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Double getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(Double currentAmount) {
        this.currentAmount = currentAmount;
    }

    public Double getCurrentDay() {
        return currentDay;
    }

    public void setCurrentDay(Double currentDay) {
        this.currentDay = currentDay;
    }

    public Double getTimeLapse() {
        return timeLapse;
    }

    public void setTimeLapse(Double timeLapse) {
        this.timeLapse = timeLapse;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getInvestType() {
        return investType;
    }

    public void setInvestType(Double investType) {
        this.investType = investType;
    }

    public Boolean getExpired() {
        return isExpired;
    }

    public void setExpired(Boolean expired) {
        isExpired = expired;
    }

    public Double getEstimated() {
        return estimated;
    }

    public void setEstimated(Double estimated) {
        this.estimated = estimated;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Boolean getRenovable() {
        return isRenovable;
    }

    public void setRenovable(Boolean renovable) {
        isRenovable = renovable;
    }

    public Double getCurrentProfit() {
        return currentProfit;
    }

    public void setCurrentProfit(Double currentProfit) {
        this.currentProfit = currentProfit;
    }

    public int getMaxRetreats() {
        return maxRetreats;
    }

    public void setMaxRetreats(int maxRetreats) {
        this.maxRetreats = maxRetreats;
    }

    public int getCurrentRetreats() {
        return currentRetreats;
    }

    public void setCurrentRetreats(int currentRetreats) {
        this.currentRetreats = currentRetreats;
    }

    public String getNextCut() {
        return nextCut;
    }

    public void setNextCut(String nextCut) {
        this.nextCut = nextCut;
    }
}
