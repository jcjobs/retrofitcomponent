package com.mobotechnology.bipinpandey.retrofit_handdirty.interactor;


import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mobotechnology.bipinpandey.retrofit_handdirty.Utilities.SecurityHandler;
import com.mobotechnology.bipinpandey.retrofit_handdirty.Utilities.Utilities;
import com.mobotechnology.bipinpandey.retrofit_handdirty.model.CUser;
import com.mobotechnology.bipinpandey.retrofit_handdirty.model.ErrorHandler;
import com.mobotechnology.bipinpandey.retrofit_handdirty.model.NoticeList;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces.GetDataServiceInterface;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces.ResultInterface;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces.ServiceConnectorInferface;
import com.mobotechnology.bipinpandey.retrofit_handdirty.network.RetrofitInstance;
import com.mobotechnology.bipinpandey.retrofit_handdirty.network.ServiceConnector;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResultInteractor implements ServiceConnectorInferface{


    public static final String Register = "0";
    public static final String Validate = "1";



    @StringDef({
            Register,
            Validate
    })
    @Retention(RetentionPolicy.SOURCE)
    public  @interface ValdationType {}



    private ResultInterface resultsInterface;

    private Context currentContext;
    public ResultInteractor(Context context) {
        this.currentContext = context;
    }


    public void makeGetNoticeList(@NonNull String session, @NonNull String token, @NonNull String cveUsuario, ResultInterface resultsInterface){
        this.resultsInterface = resultsInterface;

        String encryptedUserKey = SecurityHandler.bin2hex(SecurityHandler.getHash(cveUsuario));
        String deviceIp = Utilities.getIPAddress(true);

        Map<String, Object> params = new HashMap<>();
        params.put("session", session);
        params.put("token", token);
        params.put("ipidmovil", deviceIp);
        params.put("user", encryptedUserKey);

        ServiceConnector serviceConnector = new ServiceConnector(this, this.currentContext);
        serviceConnector.makeConnectionWithParameters("1bsqcn", params, ServiceConnector.GET);

    }





    @Override
    public void requestReturnedData(String jsonString, ErrorHandler errorHandler) {

        if(errorHandler.errorCodeString.equals("0000")){
            NoticeList noticeList = new Gson().fromJson(jsonString, NoticeList.class);
            this.resultsInterface.onResponse(noticeList.getNoticeArrayList());
        }
        else{
            this.resultsInterface.onError(errorHandler);
        }

    }



}
