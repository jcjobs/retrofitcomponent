package com.mobotechnology.bipinpandey.retrofit_handdirty.model;

import com.google.gson.annotations.SerializedName;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces.JsonRequired;

public class CUser {

    @SerializedName("numCliente")
    private String numCliente;

    @SerializedName("nombreCliente")
    private String nombreCliente;

    @SerializedName("tipoCliente")
    private String tipoCliente;

    @SerializedName("fullName")
    private String fullName;

    @SerializedName("message")
    private String message;

    @SerializedName("avatar")
    private String avatar;

    @SerializedName("lastSession")
    private String lastSession;



    @SerializedName("NombreOfuscado")
    private String obfuscatedName;

    @SerializedName("Imagen")
    private String image ;

    @SerializedName("fraseSeguridad")
    private String securityFrase;

    //@SerializedName("sessionID")
    @JsonRequired
    private String sessionID;

    public CUser(String numCliente, String nombreCliente, String tipoCliente, String fullName, String message, String avatar, String lastSession, String obfuscatedName, String image, String securityFrase, String sessionID) {
        this.numCliente = numCliente;
        this.nombreCliente = nombreCliente;
        this.tipoCliente = tipoCliente;
        this.fullName = fullName;
        this.message = message;
        this.avatar = avatar;
        this.lastSession = lastSession;
        this.obfuscatedName = obfuscatedName;
        this.image = image;
        this.securityFrase = securityFrase;
        this.sessionID = sessionID;
    }


    public String getNumCliente() {
        return numCliente;
    }

    public void setNumCliente(String numCliente) {
        this.numCliente = numCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLastSession() {
        return lastSession;
    }

    public void setLastSession(String lastSession) {
        this.lastSession = lastSession;
    }

    public String getObfuscatedName() {
        return obfuscatedName;
    }

    public void setObfuscatedName(String obfuscatedName) {
        this.obfuscatedName = obfuscatedName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSecurityFrase() {
        return securityFrase;
    }

    public void setSecurityFrase(String securityFrase) {
        this.securityFrase = securityFrase;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }
}
