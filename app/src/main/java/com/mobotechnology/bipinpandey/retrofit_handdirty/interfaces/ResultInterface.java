package com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces;

import com.mobotechnology.bipinpandey.retrofit_handdirty.model.ErrorHandler;
import com.mobotechnology.bipinpandey.retrofit_handdirty.model.Notice;

import java.util.ArrayList;

public interface ResultInterface {

    //public void showResult(ArrayList<Notice> noticeList, ErrorHandler errorHandler);
    public void onResponse(ArrayList<Notice> noticeList);
    public void onError(ErrorHandler error);

}
