package com.mobotechnology.bipinpandey.retrofit_handdirty.network;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.StringDef;
import android.util.Log;

import com.google.gson.JsonObject;
import com.mobotechnology.bipinpandey.retrofit_handdirty.Utilities.Utilities;
import com.mobotechnology.bipinpandey.retrofit_handdirty.model.ErrorHandler;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces.GetDataServiceInterface;
import com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces.ServiceConnectorInferface;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceConnector {


    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";
    @StringDef({POST, GET, PUT, DELETE})
    @Retention(RetentionPolicy.SOURCE)
    public  @interface QueryType {}



    private ServiceConnectorInferface serviceConnectorInferface;
    private Context currentContext;


    public ServiceConnector(ServiceConnectorInferface serviceConnectorInferface, Context context) {
        this.serviceConnectorInferface = serviceConnectorInferface;
        this.currentContext = context;
    }


    public void makeConnectionWithParameters(String funcionName, Map<String, Object> params, @QueryType String queryType){


        final String WS_FUNCTION = funcionName;

        switch (Utilities.getEnvironment()){
            case Utilities.SIMULATION:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        String jsonString = Utilities.loadJSONFromAsset(WS_FUNCTION+"_Response.json", currentContext);
                        Log.i("JSONResult:", jsonString);
                        serviceConnectorInferface.requestReturnedData(jsonString, new ErrorHandler(200,"0000", "OK", new Throwable()));

                    }
                }, 1000);// Millisecond 1000 = 1 sec

                return;
            default:
                break;
        }



        /** Create handle for the RetrofitInstance interface*/
        GetDataServiceInterface service = RetrofitInstance.getRetrofitInstance().create(GetDataServiceInterface.class);
        Call<JsonObject> call;


        switch (queryType){
            case POST:
                call = service.makePostConnectionWith(funcionName, params);
                break;
            default:
                call = service.makeGetConnectionWith(funcionName);
                break;
        };


        /**Log the URL called*/
        Log.wtf("URL Called", call.request().url() + "");

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                Log.i("Response:", ""+response.code());

                switch (response.code()) {
                    case 200:
                        String jsonString = response.body().toString();
                        Log.i("JSONResult:", jsonString);
                        serviceConnectorInferface.requestReturnedData(response.body().toString(), new ErrorHandler(response.code(),"0000", response.message(), new Throwable()));

                        break;
                    case 401://Bad request
                        serviceConnectorInferface.requestReturnedData(null, new ErrorHandler(response.code(),"00001",  response.message(), new Throwable()));
                        break;
                    case 400://Bad request
                        serviceConnectorInferface.requestReturnedData(null, new ErrorHandler(response.code(),"00001",  response.message(), new Throwable()));
                        break;
                    default://Bad request
                        serviceConnectorInferface.requestReturnedData(null, new ErrorHandler(response.code(),"00001",  response.message(), new Throwable()));
                        break;
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                serviceConnectorInferface.requestReturnedData(null, new ErrorHandler(500,"00001", "Something went wrong...", new Throwable()));
            }

        });

    }


}
