package com.mobotechnology.bipinpandey.retrofit_handdirty.interfaces;

import com.mobotechnology.bipinpandey.retrofit_handdirty.model.ErrorHandler;

public interface GenericResponseCallbacks {
    public void onResponse(String response);
    public void onError(ErrorHandler error);
}
